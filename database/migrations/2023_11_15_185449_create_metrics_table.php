<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('metrics', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('unit')->comment("Unité affichage");
            $table->integer('attemps')->default(0)->comment("Definie si un métric est en défault si il arrive à 3 sa définie le composant en warning et au prochain passage le déclenche en critical. (Passage du métric toutes les 30 secondes)");
            $table->float('seuil')->default(0);
            $table->float("min")->default(0);
            $table->float("max")->default(0);
            $table->boolean('active')->default(true);

            $table->foreignId('component_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('metrics');
    }
};
