<?php

namespace App\Console\Commands;

use App\Models\Datalog;
use Illuminate\Console\Command;

class MetricsPurgeCommand extends Command
{
    protected $signature = 'metrics:purge';

    protected $description = 'Command description';

    public function handle(): void
    {
        foreach (Datalog::all() as $datalog) {
            $datalog->delete();
        }
    }
}
