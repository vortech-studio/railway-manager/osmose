<?php

namespace App;

use App\Models\Component;
use App\Models\Metric;

trait MetricTrait
{
    public static function generateMetricsForComponent(Component $component, $energyType)
    {
        $metrics_config = [
            "Moteur" => [
                "vapeur" => [
                    ["name" => "Pression de vapeur", "unit" => "Pa", "min" => 100000, "max" => 300000, "seuil" => 272000],
                    ["name" => "Température de la chaudière", "unit" => "°C", "min" => 100, "max" => 500, "seuil" => 375],
                    ["name" => "Consommation d'eau", "unit" => "L/h", "min" => 10, "max" => 50, "seuil" => 39],
                ],
                "diesel" => [
                    ["name" => "Niveau de carburant", "unit" => "L", "min" => 0, "max" => 200, "seuil" => 140],
                    ["name" => "Emission", "unit" => "G/s", "min" => 0, "max" => 10, "seuil" => 8],
                ],
                "electrique" => [
                    ["name" => "Tension de la ligne", "unit" => "V", "min" => 0, "max" => 25000, "seuil" => 24500],
                    ["name" => "Courant de traction", "unit" => "A", "min" => 50, "max" => 200, "seuil" => 198],
                    ["name" => "Efficacité du moteur", "unit" => "%", "min" => 70, "max" => 100, "seuil" => 89],
                ],
                "hybride" => [
                    ["name" => "Niveau de carburant", "unit" => "L", "min" => 0, "max" => 200, "seuil" => 140],
                    ["name" => "Emission", "unit" => "G/s", "min" => 0, "max" => 10, "seuil" => 8],
                    ["name" => "Charge de Batterie", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 10],
                ],
                "autre" => [],
            ],
            "Chassis" => [
                "vapeur" => [
                    ["name" => "Vibration", "unit" => "Hz", "min" => 1, "max" => 10, "seuil" => 8],
                    ["name" => "Température du châssis", "unit" => "°C", "min" => 20, "max" => 80, "seuil" => 70],
                    ["name" => "Pression Hydraulique", "unit" => "PSI", "min" => 0, "max" => 200, "seuil" => 185],
                ],
                "diesel" => [
                    ["name" => "Vibration", "unit" => "Hz", "min" => 1, "max" => 10, "seuil" => 8],
                    ["name" => "Température du châssis", "unit" => "°C", "min" => 20, "max" => 80, "seuil" => 70],
                    ["name" => "Pression Hydraulique", "unit" => "PSI", "min" => 0, "max" => 200, "seuil" => 185],
                ],
                "electrique" => [
                    ["name" => "Vibration", "unit" => "Hz", "min" => 1, "max" => 10, "seuil" => 8],
                    ["name" => "Température du châssis", "unit" => "°C", "min" => 20, "max" => 80, "seuil" => 70],
                    ["name" => "Pression Hydraulique", "unit" => "PSI", "min" => 0, "max" => 200, "seuil" => 185],
                ],
                "hybride" => [
                    ["name" => "Vibration", "unit" => "Hz", "min" => 1, "max" => 10, "seuil" => 8],
                    ["name" => "Température du châssis", "unit" => "°C", "min" => 20, "max" => 80, "seuil" => 70],
                    ["name" => "Pression Hydraulique", "unit" => "PSI", "min" => 0, "max" => 200, "seuil" => 185],
                ],
                "autre" => [
                    ["name" => "Vibration", "unit" => "Hz", "min" => 1, "max" => 10, "seuil" => 8],
                    ["name" => "Température du châssis", "unit" => "°C", "min" => 20, "max" => 80, "seuil" => 70],
                    ["name" => "Pression Hydraulique", "unit" => "PSI", "min" => 0, "max" => 200, "seuil" => 185],
                ],
            ],
            "Transmission" => [
                "vapeur" => [
                    ["name" => "Température de transmission", "unit" => "°C", "min" => 50, "max" => 120, "seuil" => 90],
                    ["name" => "Pression de l'huile hydrolique", "unit" => "PSI", "min" => 20, "max" => 50, "seuil" => 30],
                    ["name" => "Niveau du lubrifiant", "unit" => "% optimal", "min" => 30, "max" => 80, "seuil" => 30],
                    ["name" => "Efficacité de la transmission", "unit" => "% optimal", "min" => 60, "max" => 90, "seuil" => 70],
                ],
                "diesel" => [
                    ["name" => "Température de transmission", "unit" => "°C", "min" => 50, "max" => 120, "seuil" => 90],
                    ["name" => "Pression de l'huile hydrolique", "unit" => "PSI", "min" => 20, "max" => 50, "seuil" => 30],
                    ["name" => "Niveau du lubrifiant", "unit" => "% optimal", "min" => 30, "max" => 80, "seuil" => 30],
                    ["name" => "Efficacité de la transmission", "unit" => "% optimal", "min" => 60, "max" => 90, "seuil" => 70],
                ],
                "electrique" => [
                    ["name" => "Température de transmission", "unit" => "°C", "min" => 50, "max" => 120, "seuil" => 90],
                    ["name" => "Pression de l'huile hydrolique", "unit" => "PSI", "min" => 20, "max" => 50, "seuil" => 30],
                    ["name" => "Niveau du lubrifiant", "unit" => "% optimal", "min" => 30, "max" => 80, "seuil" => 30],
                    ["name" => "Efficacité de la transmission", "unit" => "% optimal", "min" => 60, "max" => 90, "seuil" => 70],
                ],
                "hybride" => [
                    ["name" => "Température de transmission", "unit" => "°C", "min" => 50, "max" => 120, "seuil" => 90],
                    ["name" => "Pression de l'huile hydrolique", "unit" => "PSI", "min" => 20, "max" => 50, "seuil" => 30],
                    ["name" => "Niveau du lubrifiant", "unit" => "% optimal", "min" => 30, "max" => 80, "seuil" => 30],
                    ["name" => "Efficacité de la transmission", "unit" => "% optimal", "min" => 60, "max" => 90, "seuil" => 70],
                ],
                "autre" => [],
            ],
            "Bogie/Essieux" => [
                "vapeur" => [
                    ["name" => "Température des essieux", "unit" => "°C", "min" => 0, "max" => 95, "seuil" => 70],
                    ["name" => "Niveau de vibration des essieux", "unit" => "Hz", "min" => 5, "max" => 50, "seuil" => 39],
                    ["name" => "Usure des roues", "unit" => "%", "min" => 10, "max" => 50, "seuil" => 30],
                    ["name" => "Pression Pneumatique", "unit" => "PSI", "min" => 80, "max" => 120, "seuil" => 110],
                    ["name" => "Température des freins", "unit" => "°C", "min" => 0, "max" => 120, "seuil" => 90],
                    ["name" => "Efficacité des amortisseurs", "unit" => "%", "min" => 70, "max" => 90, "seuil" => 80],
                ],
                "diesel" => [
                    ["name" => "Température des essieux", "unit" => "°C", "min" => 0, "max" => 95, "seuil" => 70],
                    ["name" => "Niveau de vibration des essieux", "unit" => "Hz", "min" => 5, "max" => 50, "seuil" => 39],
                    ["name" => "Usure des roues", "unit" => "%", "min" => 10, "max" => 50, "seuil" => 30],
                    ["name" => "Pression Pneumatique", "unit" => "PSI", "min" => 80, "max" => 120, "seuil" => 110],
                    ["name" => "Température des freins", "unit" => "°C", "min" => 0, "max" => 120, "seuil" => 90],
                    ["name" => "Efficacité des amortisseurs", "unit" => "%", "min" => 70, "max" => 90, "seuil" => 80],
                ],
                "electrique" => [
                    ["name" => "Température des essieux", "unit" => "°C", "min" => 0, "max" => 95, "seuil" => 70],
                    ["name" => "Niveau de vibration des essieux", "unit" => "Hz", "min" => 5, "max" => 50, "seuil" => 39],
                    ["name" => "Usure des roues", "unit" => "%", "min" => 10, "max" => 50, "seuil" => 30],
                    ["name" => "Pression Pneumatique", "unit" => "PSI", "min" => 80, "max" => 120, "seuil" => 110],
                    ["name" => "Température des freins", "unit" => "°C", "min" => 0, "max" => 120, "seuil" => 90],
                    ["name" => "Efficacité des amortisseurs", "unit" => "%", "min" => 70, "max" => 90, "seuil" => 80],
                ],
                "hybride" => [
                    ["name" => "Température des essieux", "unit" => "°C", "min" => 0, "max" => 95, "seuil" => 70],
                    ["name" => "Niveau de vibration des essieux", "unit" => "Hz", "min" => 5, "max" => 50, "seuil" => 39],
                    ["name" => "Usure des roues", "unit" => "%", "min" => 10, "max" => 50, "seuil" => 30],
                    ["name" => "Pression Pneumatique", "unit" => "PSI", "min" => 80, "max" => 120, "seuil" => 110],
                    ["name" => "Température des freins", "unit" => "°C", "min" => 0, "max" => 120, "seuil" => 90],
                    ["name" => "Efficacité des amortisseurs", "unit" => "%", "min" => 70, "max" => 90, "seuil" => 80],
                ],
                "autre" => [
                    ["name" => "Température des essieux", "unit" => "°C", "min" => 0, "max" => 95, "seuil" => 70],
                    ["name" => "Niveau de vibration des essieux", "unit" => "Hz", "min" => 5, "max" => 50, "seuil" => 39],
                    ["name" => "Usure des roues", "unit" => "%", "min" => 10, "max" => 50, "seuil" => 30],
                    ["name" => "Pression Pneumatique", "unit" => "PSI", "min" => 80, "max" => 120, "seuil" => 110],
                    ["name" => "Température des freins", "unit" => "°C", "min" => 0, "max" => 120, "seuil" => 90],
                    ["name" => "Efficacité des amortisseurs", "unit" => "%", "min" => 70, "max" => 90, "seuil" => 80],
                ],
            ],
            "Systeme de freinage" => [
                "vapeur" => [
                    ["name" => "Température des freins", "unit" => "°C", "min" => 0, "max" => 200, "seuil" => 130],
                    ["name" => "Pression des freins", "unit" => "PSI", "min" => 90, "max" => 120, "seuil" => 100],
                    ["name" => "Épaisseur des plaquettes", "unit" => "mm", "min" => 10, "max" => 20, "seuil" => 11],
                    ["name" => "Usure des disque", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 25],
                    ["name" => "Effort de freinage", "unit" => "PSI", "min" => 50, "max" => 120, "seuil" => 95],
                ],
                "diesel" => [
                    ["name" => "Température des freins", "unit" => "°C", "min" => 0, "max" => 200, "seuil" => 130],
                    ["name" => "Pression des freins", "unit" => "PSI", "min" => 90, "max" => 120, "seuil" => 100],
                    ["name" => "Épaisseur des plaquettes", "unit" => "mm", "min" => 10, "max" => 20, "seuil" => 11],
                    ["name" => "Usure des disque", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 25],
                    ["name" => "Effort de freinage", "unit" => "PSI", "min" => 50, "max" => 120, "seuil" => 95],
                ],
                "electrique" => [
                    ["name" => "Température des freins", "unit" => "°C", "min" => 0, "max" => 200, "seuil" => 130],
                    ["name" => "Pression des freins", "unit" => "PSI", "min" => 90, "max" => 120, "seuil" => 100],
                    ["name" => "Épaisseur des plaquettes", "unit" => "mm", "min" => 10, "max" => 20, "seuil" => 11],
                    ["name" => "Usure des disque", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 25],
                    ["name" => "Effort de freinage", "unit" => "PSI", "min" => 50, "max" => 120, "seuil" => 95],
                ],
                "hybride" => [
                    ["name" => "Température des freins", "unit" => "°C", "min" => 0, "max" => 200, "seuil" => 130],
                    ["name" => "Pression des freins", "unit" => "PSI", "min" => 90, "max" => 120, "seuil" => 100],
                    ["name" => "Épaisseur des plaquettes", "unit" => "mm", "min" => 10, "max" => 20, "seuil" => 11],
                    ["name" => "Usure des disque", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 25],
                    ["name" => "Effort de freinage", "unit" => "PSI", "min" => 50, "max" => 120, "seuil" => 95],
                ],
                "autre" => [
                    ["name" => "Température des freins", "unit" => "°C", "min" => 0, "max" => 200, "seuil" => 130],
                    ["name" => "Pression des freins", "unit" => "PSI", "min" => 90, "max" => 120, "seuil" => 100],
                    ["name" => "Épaisseur des plaquettes", "unit" => "mm", "min" => 10, "max" => 20, "seuil" => 11],
                    ["name" => "Usure des disque", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 25],
                    ["name" => "Effort de freinage", "unit" => "PSI", "min" => 50, "max" => 120, "seuil" => 95],
                ],
            ],
            "Caisse" => [
                "vapeur" => [
                    ["name" => "Déformation de la caisse", "unit" => "mm", "min" => 0, "max" => 50, "seuil" => 30],
                    ["name" => "Vibration de la caisse", "unit" => "Hz", "min" => 0, "max" => 10, "seuil" => 8],
                    ["name" => "Niveau sonore dans la caisse", "unit" => "dB", "min" => 0, "max" => 95, "seuil" => 70],
                ],
                "diesel" => [
                    ["name" => "Déformation de la caisse", "unit" => "mm", "min" => 0, "max" => 50, "seuil" => 30],
                    ["name" => "Vibration de la caisse", "unit" => "Hz", "min" => 0, "max" => 10, "seuil" => 8],
                    ["name" => "Niveau sonore dans la caisse", "unit" => "dB", "min" => 0, "max" => 95, "seuil" => 70],
                ],
                "electrique" => [
                    ["name" => "Déformation de la caisse", "unit" => "mm", "min" => 0, "max" => 50, "seuil" => 30],
                    ["name" => "Vibration de la caisse", "unit" => "Hz", "min" => 0, "max" => 10, "seuil" => 8],
                    ["name" => "Niveau sonore dans la caisse", "unit" => "dB", "min" => 0, "max" => 95, "seuil" => 70],
                ],
                "hybride" => [
                    ["name" => "Déformation de la caisse", "unit" => "mm", "min" => 0, "max" => 50, "seuil" => 30],
                    ["name" => "Vibration de la caisse", "unit" => "Hz", "min" => 0, "max" => 10, "seuil" => 8],
                    ["name" => "Niveau sonore dans la caisse", "unit" => "dB", "min" => 0, "max" => 95, "seuil" => 70],
                ],
                "autre" => [
                    ["name" => "Déformation de la caisse", "unit" => "mm", "min" => 0, "max" => 50, "seuil" => 30],
                    ["name" => "Vibration de la caisse", "unit" => "Hz", "min" => 0, "max" => 10, "seuil" => 8],
                    ["name" => "Niveau sonore dans la caisse", "unit" => "dB", "min" => 0, "max" => 95, "seuil" => 70],
                ]
            ],
            "Système d'éclairage intérieur" => [
                "vapeur" => [],
                "diesel" => [],
                "electrique" => [
                    ["name" => "Intensité lumineuse", "unit" => "lx", "min" => 100, "max" => 500, "seuil" => 230],
                    ["name" => "Uniformité lumineuse", "unit" => "%", "min" => 60, "max" => 100, "seuil" => 75],
                ],
                "hybride" => [
                    ["name" => "Intensité lumineuse", "unit" => "lx", "min" => 100, "max" => 500, "seuil" => 230],
                    ["name" => "Uniformité lumineuse", "unit" => "%", "min" => 60, "max" => 100, "seuil" => 75],
                ],
                "autre" => [
                    ["name" => "Intensité lumineuse", "unit" => "lx", "min" => 100, "max" => 500, "seuil" => 230],
                    ["name" => "Uniformité lumineuse", "unit" => "%", "min" => 60, "max" => 100, "seuil" => 75],
                ],
            ],
            "Système de climatisation/chauffage" => [
                "vapeur" => [
                    ["name" => "Température de l'air", "unit" => "°C", "min" => 18, "max" => 30, "seuil" => 24],
                    ["name" => "Niveau sonore du système", "unit" => "dB", "min" => 0, "max" => 65, "seuil" => 50],
                    ["name" => "Débit d'air", "unit" => "CFM", "min" => 200, "max" => 600, "seuil" => 480],
                ],
                "diesel" => [
                    ["name" => "Température de l'air", "unit" => "°C", "min" => 18, "max" => 30, "seuil" => 24],
                    ["name" => "Niveau sonore du système", "unit" => "dB", "min" => 0, "max" => 65, "seuil" => 50],
                    ["name" => "Débit d'air", "unit" => "CFM", "min" => 200, "max" => 600, "seuil" => 480],
                ],
                "electrique" => [
                    ["name" => "Température de l'air", "unit" => "°C", "min" => 18, "max" => 30, "seuil" => 24],
                    ["name" => "Niveau sonore du système", "unit" => "dB", "min" => 0, "max" => 65, "seuil" => 50],
                    ["name" => "Débit d'air", "unit" => "CFM", "min" => 200, "max" => 600, "seuil" => 480],
                ],
                "hybride" => [
                    ["name" => "Température de l'air", "unit" => "°C", "min" => 18, "max" => 30, "seuil" => 24],
                    ["name" => "Niveau sonore du système", "unit" => "dB", "min" => 0, "max" => 65, "seuil" => 50],
                    ["name" => "Débit d'air", "unit" => "CFM", "min" => 200, "max" => 600, "seuil" => 480],
                ],
                "autre" => [
                    ["name" => "Température de l'air", "unit" => "°C", "min" => 18, "max" => 30, "seuil" => 24],
                    ["name" => "Niveau sonore du système", "unit" => "dB", "min" => 0, "max" => 65, "seuil" => 50],
                    ["name" => "Débit d'air", "unit" => "CFM", "min" => 200, "max" => 600, "seuil" => 480],
                ]
            ],
            "Système de communication" => [
                "vapeur" => [],
                "diesel" => [
                    ["name" => "Intelligibilité vocale", "unit" => "IV", "min" => 0, "max" => 1, "seuil" => 0.6],
                    ["name" => "Taux de transfères", "unit" => "kbps", "min" => 0, "max" => 512, "seuil" => 128],
                    ["name" => "Redondance du système", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 90],
                    ["name" => "Résistance aux interférences électromagnétiques (EMI)", "unit" => "dB", "min" => 0, "max" => 100, "seuil" => 80],
                ],
                "electrique" => [
                    ["name" => "Intelligibilité vocale", "unit" => "IV", "min" => 0, "max" => 1, "seuil" => 0.6],
                    ["name" => "Taux de transfères", "unit" => "kbps", "min" => 0, "max" => 512, "seuil" => 128],
                    ["name" => "Redondance du système", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 90],
                    ["name" => "Résistance aux interférences électromagnétiques (EMI)", "unit" => "dB", "min" => 0, "max" => 100, "seuil" => 80],
                ],
                "hybride" => [
                    ["name" => "Intelligibilité vocale", "unit" => "IV", "min" => 0, "max" => 1, "seuil" => 0.6],
                    ["name" => "Taux de transfères", "unit" => "kbps", "min" => 0, "max" => 512, "seuil" => 128],
                    ["name" => "Redondance du système", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 90],
                    ["name" => "Résistance aux interférences électromagnétiques (EMI)", "unit" => "dB", "min" => 0, "max" => 100, "seuil" => 80],
                ],
                "autre" => [
                    ["name" => "Intelligibilité vocale", "unit" => "IV", "min" => 0, "max" => 1, "seuil" => 0.6],
                    ["name" => "Taux de transfères", "unit" => "kbps", "min" => 0, "max" => 512, "seuil" => 128],
                    ["name" => "Redondance du système", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 90],
                    ["name" => "Résistance aux interférences électromagnétiques (EMI)", "unit" => "dB", "min" => 0, "max" => 100, "seuil" => 80],
                ]
            ],
            "Chauffe-eau" => [
                "vapeur" => [
                    ["name" => "Pression de fonctionnement", "unit" => "bar", "min" => 10, "max" => 20, "seuil" => 15],
                    ["name" => "Température de fonctionnement", "unit" => "°C", "min" => 150, "max" => 250, "seuil" => 200],
                    ["name" => "Débit de vapeur produit", "unit" => "L/min", "min" => 25, "max" => 180, "seuil" => 125],
                    ["name" => "Niveau d'eau dans le réservoir", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 20],
                ],
                "diesel" => [],
                "electrique" => [],
                "hybride" => [],
                "autre" => []
            ],
            "Réservoir" => [
                "diesel" => [
                    ["name" => "Niveau du carburant", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 20],
                    ["name" => "Pression dans le réservoir", "unit" => "Pa", "min" => 100000, "max" => 300000, "seuil" => 250000],
                    ["name" => "Température du Carburant", "unit" => "°C", "min" => -10, "max" => 50, "seuil" => 45],
                ],
                "hybride" => [
                    ["name" => "Niveau du carburant", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 20],
                    ["name" => "Pression dans le réservoir", "unit" => "Pa", "min" => 100000, "max" => 300000, "seuil" => 250000],
                    ["name" => "Température du Carburant", "unit" => "°C", "min" => -10, "max" => 50, "seuil" => 45],
                ],
                "electrique" => [

                ],
                "vapeur" => [

                ],
                "autre" => []
            ],
            "Transformateur" => [
                "electrique" => [
                    ["name" => "Température du transformateur", "unit" => "°C", "min" => 50, "max" => 100, "seuil" => 95],
                    ["name" => "Niveau d'Isolation", "unit" => "V", "min" => 1000, "max" => 3000, "seuil" => 800],
                    ["name" => "Efficacite Energétique", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 85],
                    ["name" => "Tension de Sortie", "unit" => "kV", "min" => 5, "max" => 25, "seuil" => 8],
                    ["name" => "Courant de Charge", "unit" => "A", "min" => 50, "max" => 350, "seuil" => 200],
                ],
                "hybride" => [
                    ["name" => "Température du transformateur", "unit" => "°C", "min" => 50, "max" => 100, "seuil" => 95],
                    ["name" => "Niveau d'Isolation", "unit" => "V", "min" => 1000, "max" => 3000, "seuil" => 800],
                    ["name" => "Efficacite Energétique", "unit" => "%", "min" => 0, "max" => 100, "seuil" => 85],
                    ["name" => "Tension de Sortie", "unit" => "kV", "min" => 5, "max" => 25, "seuil" => 8],
                    ["name" => "Courant de Charge", "unit" => "A", "min" => 50, "max" => 350, "seuil" => 200],
                ],
                "vapeur" => [],
                "diesel" => [],
                "autre" => [],
            ]
        ];

        if(isset($metrics_config[$component->name]) || isset($metrics_config[$component->name][$energyType])) {
            if(isset($metrics_config[$component->name][$energyType])) {
                $metrics = $metrics_config[$component->name][$energyType];

            } else {
                $metrics = $metrics_config[$component->name];

            }
            foreach ($metrics as $metric) {
                \Log::debug("Log System Install Engine OSMOSE:", [
                    "metric" => $metric,
                    "component" => $component,
                    "energy_type" => $energyType
                ]);
                Metric::create([
                    "name" => $metric["name"],
                    "unit" => $metric["unit"],
                    "min" => $metric["min"],
                    "max" => $metric["max"],
                    "seuil" => $metric["seuil"],
                    "component_id" => $component->id
                ]);
            }

            return $metrics;
        }

        return [];
    }
}
