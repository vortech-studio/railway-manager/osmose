<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $appends = ['name_slugify', 'status_color', 'status_text'];

    protected $casts = [
        'latest_update' => 'datetime',
    ];

    public function engine()
    {
        return $this->belongsTo(Engine::class);
    }

    public function metrics()
    {
        return $this->hasMany(Metric::class);
    }

    public function getNameSlugifyAttribute()
    {
        return $this->getSlugifyName();
    }

    public function getSlugifyName()
    {
        return match($this->name) {
            "Chassis" => "chassis",
            "Transmission" => "transmission",
            "Bogie/Essieux" => "bogie",
            "Systeme de freinage" => "frein",
            "Caisse" => "caisse",
            "Sièges" => "siege",
            "Système d'éclairage intérieur" => "eclairage",
            "Système de climatisation/chauffage" => "climatisation",
            "Système de communication" => "communication",
            "Fenêtres" => "window",
            "Moteur" => "moteur",
            "Réservoir" => "reservoir",
            "Transformateur" => "transformateur",
            "Chauffe-eau" => "chauffe-eau",
            default => $this->name,
        };
    }

    public function getStatusColorAttribute()
    {
        return match ($this->status) {
            "stable" => "success",
            "warning" => "warning",
            "critical" => "danger",
            default => "secondary"
        };
    }

    public function getStatusTextAttribute()
    {
        return match ($this->status) {
            "stable" => "Stable",
            "warning" => "Attention",
            "critical" => "Critique",
            default => "Inconnue"
        };
    }
}
