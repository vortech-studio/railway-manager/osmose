<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Engine extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function components()
    {
        return $this->hasMany(Component::class);
    }
}
