<?php

namespace App;

trait ComponentTrait
{
    public static function get($type, $energy)
    {
        $commonComponents = ["Chassis", "Transmission", "Bogie/Essieux", "Systeme de freinage"];
        $engineComponents = [];
        $passengerComponents = ['Caisse', 'Sièges', 'Système d\'éclairage intérieur', 'Système de climatisation/chauffage', 'Système de communication', 'Fenêtres'];

        switch ($type) {
            case 'automotrice':
            case "motrice":
                $engineComponents = ["Moteur"];
                break;

            case "remorque":
                $engineComponents = [];
                break;

            default:
                break;
        }

        switch ($energy) {
            case 'vapeur':
                $engineComponents[] = "Chauffe-eau";
                break;

            case 'diesel':
                $engineComponents[] = "Réservoir";
                break;

            case "electrique":
                $engineComponents[] = "Transformateur";
                break;

            case 'hybride':
                $engineComponents[] = "Réservoir";
                $engineComponents[] = "Transformateur";
                break;

            default:
                break;
        }

        return array_merge($commonComponents, $engineComponents, $passengerComponents);
    }
}
