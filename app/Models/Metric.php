<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Metric extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    protected $appends = ['latest_logs_value', 'latest_logs_date', 'latest_logs_color'];

    public function component()
    {
        return $this->belongsTo(Component::class);
    }

    public function logs()
    {
        return $this->hasMany(Datalog::class);
    }

    public function getLatestLogsValueAttribute()
    {
        return $this->logs()->latest()->first()->value ?? null;
    }

    public function getLatestLogsDateAttribute()
    {
        return $this->logs()->latest()->first()->created_at->format("d/m/Y H:i") ?? null;
    }

    public function getLatestLogsColorAttribute()
    {
        $log = $this->logs()->latest()->first();

        if($log->value > $this->seuil) {
            return "danger";
        } elseif ($log->value < $this->seuil) {
            return "warning";
        } else {
            return "success";
        }
    }
}
