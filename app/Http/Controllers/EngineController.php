<?php

namespace App\Http\Controllers;

use App\ComponentTrait;
use App\MetricTrait;
use App\Models\Engine;
use Illuminate\Http\Request;

class EngineController extends Controller
{

    public function __construct()
    {
    }

    public function store(Request $request)
    {

        $engine = Engine::create([
            "railway_user_engine_id" => $request->get('engine_id')
        ]);

        $components = ComponentTrait::get($request->get('type_materiel'), $request->get('type_moteur'));

        foreach ($components as $component) {
            $compo = $engine->components()->create([
                "name" => $component,
                "engine_id" => $engine->id
            ]);

            MetricTrait::generateMetricsForComponent($compo, $request->get('type_moteur'));
        }

        return response()->json($engine->with('components')->orderBy('id', 'desc')->first());

    }

    public function show($user_engine_id)
    {
        $engine = Engine::with('components')->where('railway_user_engine_id', $user_engine_id)->first();
        $components = [];

        foreach ($engine->components as $component) {
            $metrics = [];

            $components[] = [
                "name" => $component->name,
                "slugify" => $component->getSlugifyName(),
                "metrics" => $metrics
            ];

            foreach ($component->metrics as $metric) {
                $logs = [];
                $metrics[] = [
                    "name" => $metric->name,
                    "logs" => $logs
                ];

                foreach ($metric->logs as $log) {
                    $logs[] = [
                        "date" => $log->created_at,
                        "value" => $log->value
                    ];
                }
            }
        }


        return response()->json([
            "engine" => $engine,
            "operationnal" => $this->getOperationnalStatusEngine($engine)
        ]);
    }

    private function getOperationnalStatusEngine(Engine $engine)
    {
        $count_stable = $engine->components()->where('status', 'stable')->count();
        $count_warning = $engine->components()->where('status', 'warning')->count();
        $count_critical = $engine->components()->where('status', 'critical')->count();

        if($count_stable > $count_warning && $count_stable > $count_critical) {
            return [
                "value" => "Opérationnel",
                "color" => "success",
            ];
        }
        if($count_warning > $count_stable && $count_warning > $count_critical) {
            return [
                "value" => "Attention requise",
                "color" => "warning",
            ];
        }
        if($count_critical > $count_stable && $count_critical > $count_warning) {
            return [
                "value" => "Intervention requise",
                "color" => "danger",
            ];
        }


    }
}
