<?php

namespace App\Console\Commands;

use App\Models\Component;
use App\Models\Datalog;
use App\Models\Metric;
use Illuminate\Console\Command;

class GenerateMetricsCommand extends Command
{
    protected $signature = 'metrics:generate';

    protected $description = 'Generate and store metric values every 30 seconds';

    public function handle(): void
    {
        $metrics = Metric::where('active', true)->get();

        foreach ($metrics as $metric) {
            $value = $this->generateRandomValue($metric);

            Datalog::create([
                "metric_id" => $metric->id,
                "value" => $value
            ]);

            if($metric->attemps == 3) {
                $metric->component->update(["status" => "warning"]);
            } elseif ($metric->attemps >= 5) {
                $metric->component->update(["status" => "critical"]);
                $metric->update(["active" => false]);
            }
        }
    }

    private function generateRandomValue(mixed $metric)
    {
        $min = $metric->min;
        $max = $metric->max;
        $seuil = $metric->seuil;

        $value = rand($min, $max);

        if($value > $seuil) {
            $metric->increment('attemps');
        }

        return $value;
    }
}
